---
layout: handbook-page-toc
title: Real-time Editing of Issue Descriptions (REID) Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Real-time Editing of Issue Descriptions (REID) Single-Engineer Group

The Real-time Editing of Issue Descriptions (REID) SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation/).

This group is focussed on Real-time collaborative editing of issue descriptions and will build upon the recent work to [view real-time updates of assignee in issue sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589).

The goal is to have real-time collaborative editing of issue descriptions in order to turn issues into a Google Docs type of experience.  Initially we will dogfood this at GitLab for meeting agendas and notes.

There are additional ideas within the [Real-time collaboration Epic](https://gitlab.com/groups/gitlab-org/-/epics/2345), however the goal of this SEG is the minimal functionality described above.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/21473](https://gitlab.com/gitlab-org/gitlab/-/issues/21473)

### Reading List

* [Hybrid Anxiety and Hybrid Optimism: The Near Future of Work](https://future.a16z.com/hybrid-anxiety-optimism-future-of-work/)
