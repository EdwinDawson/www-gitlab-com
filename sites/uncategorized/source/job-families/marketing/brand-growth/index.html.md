---
layout: job_family_page
title: "Brand Growth - Roles & Responsibilities"
---

## Levels

### Manager, Brand Strategy and Growth

The Manager, Brand Strategy and Growth reports to the Senior Director, Brand.

#### Manager, Brand Strategy and Growth Job Grade

The Manager, Brand Strategy and Growth is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Manager, Brand Strategy and Growth Responsibilities

* Oversee a team of brand managers to deliver brand campaigns in line with our messaging to drive unaided and aided brand awareness. 
* Provides overall brand strategy to drive brand growth. 
* Creates projections of media buy spend in line with brand growth goals. 
* Works collaboratively with key stakeholders to create brand messaging.
* Works cross-functionally across sales and marketing to make sure brand campaigns create as much impact as possible. 
* Educate and communicate our brand personality internally and align company around the message.
* Startegically works with partners, customers and other third-parties to figure out ways to uplevel the GitLab brand. 
* Develops global brand campaign strategy to impact markets of interest to GitLab. 
* Oversees and guides the team on KPIs for brand campaigns and impact on overall brand. 

#### Manager, Brand Strategy and Growth Requirements

* Ability to use GitLab
* Proven and demonstrated progressive experience in a management role.
* Experienced brand strategist with expertise delivering global brand campaigns. 
* Ability to think outside the box to uplevel the GitLab brand in new and unique ways. 
* Advanced knowledge on brand measurement and tools for brand measurement. 
* Proven capability to coordinate across many teams and perform in a fast-moving startup environment.

### Staff Brand Manager

The Staff Brand Manager reports to the Manager, Brand Strategy and Growth.

#### Staff Brand Manager Job Grade

The Staff Brand Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

**Market Justification:** The Staff Brand Manager provides a high level of expertise in brand, marketing, and strategy to develop integrated marketing campaigns in an effort to increase GitLab’s brand awareness. Additionally, this role is specialized due to industry experience required to collaborate with high-profile individuals, agencies, executives, and other senior stakeholders. This role will help shape GitLab’s brand through collaboration and iteration. A comparable role at GitLab is the [Staff Brand Designer](https://about.gitlab.com/job-families/marketing/brand-designer/#staff-brand-designer).

#### Staff Brand Manager Responsibilities

* Identify how our brand is currently positioned in the market and develop a reporting process to document the progress of our brand
* Implement brand measurement systems so the value of our brand can be easily tracked.
* Oversee the brand message creation process, as well as make sure the copy on our web properties align with the message.
* Develop and execute marketing campaigns aimed at communicating our brand message.
* Work cross collaboratively to educate and communicate our brand personality internally and align company around the message.
* Help evolve and maintain GitLab's brand positioning, how it’s articulated, how it is best deployed, across all marketing touchpoints.
* Partner with brand design team to plan, manage, and implement a calendar of brand opportunities to bring the GitLab brand to life. 
* Measure and report on success of brand marketing campaigns.
* Oversee merchandise management.
* Provide support with brand team process establishment and documentation.

#### Staff Brand Manager Requirements

* Ability to use GitLab.
* Experience in a brand manager role or similar acting role. 
* Knowledge of brand tracking and brand measurement systems.
* Capability to coordinate across many teams and perform in fast-moving startup environment.
* Understand and be able to express the value of GitLab as a brand entity.
* Outstanding written and verbal communications skills.
* You embrace our values, and work in accordance with those values.
* Develop pre-event, at-event, and post-event marketing plans in conjunction with product marketing, content, alliances, demand generation and field teams.
* Advance, maintain, and manage external partners and vendors for multiple events and swag, including negotiating optimal terms.
* Partner with internal cross-functional teams to create event agendas, campaigns, and content that meet engagement targets.
* Oversee creation of event communications and promotion for event programs, in partnership with demand generation and sales development teams, to drive attendance.
* Develop and deliver event enablement to include pre-event briefings, at-event briefings, and drive post-event follow-up briefings and event retrospectives.

## Performance Indicators

* Executing overall brand strategy and brand campaign initiatives 
* Collaborating effectively and receiving overall buy-in internally 
* Reporting on metrics on brand campaigns to show that our brand is trending upwards. 

## Career Ladder

* The next step in the Corporate Events job family is not yet defined at GitLab.

### Director, Certification Brand 

The Director, Certification Brand will work closely with the customer success team to develop the certification brand and drive corporate marketing efforts to create awareness of the GitLab Certification program. This role reports directly to the Senior Director, Brand.

#### Director, Certification Brand Job Grade

The Director, Certification Brand role is a [grade 10](https://about.gitlab.com/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades)

#### Director, Certification Brand Responsibilities 

* Create strategy and oversee brand creation assets and naming for the certification strategy.
* Work directly with the brand agency to coordinate between teams and drive to timely and data-driven decisions.
* Own the message house and positioning for the certification program to elevate DevOps professionals careers.
* Develop brand and corporate marketing campaigns to create awareness of the certification program. 
* Collaborate with key stakeholders and leadership to meet deadlines.
* Provide data on how the certification brand is measured compared to other competitive programs. 
* Create cross-functional marketing plans to drive demand of the program. 
* Work hand-in-hand with the contet marketig team to develop content to promote the certification programs.
* Think holistically to work with external certification programs to drive interest in GitLab's certification program.

#### Director, Certification Brand Requirements

* Experience working on the marketing elements of a certification program, prior DevOps experience a plus.
* Ability to drive consensus and influence across a large group of stakeholders.
* Manage multiple priorities as an independent, self-starter with excellent time management within a rapidly evolving business.
* A general marketer at heart with a key understanding of public relations, content marketing, social, event marketing and high-level demand gen and marketing ops needs. 
* Ability to iterate on overall messaging and positioning as the certification program evolves. 
* Brand measurement experience.
* Demonstrated track record in crafting compelling positioning and marketing content to support high-level launches.
* A bias for action for developing awesome marketing assets (website, videos, success stories, blogs, print collateral).
* Experience working in a fast-paced and highly cross-functional organization.

## Performanc Indicators

* The number of sign-ups to the certification program 
* Measuring the social sharing of the certification program 
* Outlining the certification funnel and activities to match each stage of the funnel
* Increasing brand awareness of the program

## Hiring process

Candidates for these positions can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the Hiring Manager
* Next, candidates will be invited to interview with 2-5 team members
* There may be a final executive interview

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
