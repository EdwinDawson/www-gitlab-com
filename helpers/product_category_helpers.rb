require 'kramdown'

module ProductCategoryHelpers
  def category_groups
    @category_groups ||= data.stages.stages.flat_map do |_, stage|
      Array(stage.groups).flat_map do |_, group|
        Array(group.categories).map { |category| [category, group] }
      end
    end.to_h
  end

  def category_group_link(category_slug)
    if group = category_groups[category_slug]
      "[#{group.name}](##{group_id(group)})"
    else
      'Not owned'
    end
  end

  def group_id(group)
    Kramdown::Converter::Base.send(:new, nil, nil).basic_generate_id("#{group.name} group")
  end
end
